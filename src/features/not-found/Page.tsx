import Header from "../../common/page-parts/Header";

export default function NotFoundPage() {
    return (
        <div>
            Page not found
        </div>
    );
}
